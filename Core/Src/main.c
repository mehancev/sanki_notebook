/* USER CODE BEGIN Header */
/*
 * main.c
 *
 *  Created on: 15 sep 2021
 *      Author: Author: ssdmt@mail.ru, cyb.ssdmt@gmail.com
 */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "pc.h"
#include "uart_util.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
UART_HandleTypeDef huart1;
UART_HandleTypeDef huart3;
DMA_HandleTypeDef hdma_usart3_tx;

/* USER CODE BEGIN PV */
uint8_t f_start=0;

uint8_t gsens_X[10];
uint8_t gsens_Y[10];
uint8_t gsens_Z[10];

uint8_t chan = 001;
uint8_t chan_buf[12];
uint8_t chan_buf2[12];
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_USART1_UART_Init(void);
static void MX_DMA_Init(void);
static void MX_USART3_UART_Init(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */
extern uint8_t gnss_uart_cycle();
extern uint32_t _sys_tick;
extern uint8_t gsens_buf[25];

extern uint8_t gsens_uart_cycle();
extern float gsens_uart_parse(uint32_t len, UART_HandleTypeDef *huart_pc /*DEBUG*/);
extern uint8_t pc_uart_cycle();
extern uint8_t pc_uart_parse(uint32_t len, UART_HandleTypeDef *huart_pc);

void reset_chan_buf()
{
	chan_buf[0] = 0; chan_buf[1] = 0; chan_buf[2] = 0; chan_buf[3] = 0;
	chan_buf[4] = 0; chan_buf[5] = 0; chan_buf[6] = 0; chan_buf[7] = 0;
	chan_buf[8] = 0; chan_buf[9] = 0; chan_buf[10] = 0; chan_buf[11] = 0;
}

void reset_chan_buf2()
{
	chan_buf2[0] = 0; chan_buf2[1] = 0; chan_buf2[2] = 0; chan_buf2[3] = 0;
	chan_buf2[4] = 0; chan_buf2[5] = 0; chan_buf2[6] = 0; chan_buf[7] = 0;
	chan_buf2[8] = 0; chan_buf2[9] = 0; chan_buf2[10] = 0; chan_buf2[11] = 0;
}

void reset_JDY_40()
{
	  HAL_GPIO_WritePin(JDY_PW1_GPIO_Port, JDY_PW1_Pin, GPIO_PIN_RESET);
	  HAL_GPIO_WritePin(JDY_PW2_GPIO_Port, JDY_PW2_Pin, GPIO_PIN_RESET);
	  HAL_GPIO_WritePin(JDY_PW3_GPIO_Port, JDY_PW3_Pin, GPIO_PIN_RESET);
	  HAL_Delay(800);
	  HAL_GPIO_WritePin(JDY_PW1_GPIO_Port, JDY_PW1_Pin, GPIO_PIN_SET);
	  HAL_GPIO_WritePin(JDY_PW2_GPIO_Port, JDY_PW2_Pin, GPIO_PIN_SET);
	  HAL_GPIO_WritePin(JDY_PW3_GPIO_Port, JDY_PW3_Pin, GPIO_PIN_SET);
	  HAL_Delay(800);
}

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_USART1_UART_Init();
  MX_DMA_Init();
  MX_USART3_UART_Init();
  /* USER CODE BEGIN 2 */
  uint8_t cmd=0;
  uint8_t gsens_len=0; // GSens sentence len UART1
  uint8_t pc_len=0; // PC sentence len UART2
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  uart_start(&huart1); // g-Sens
  uart_start(&huart3); // PC

  while (1)
  {
	  // G-SENSOR rx-uart
	  if(f_start) {
		  gsens_len = gsens_uart_cycle();
		  if (gsens_len && f_start) {
			  HAL_UART_Transmit_DMA(&huart3, gsens_buf, gsens_len);
			  gsens_len = 0;
		  }
	  }

	  // PC rx-uart
	  pc_len = pc_uart_cycle();
	  if (pc_len) {
		  cmd = pc_uart_parse(pc_len, &huart3/*DEBUG*/);
		  switch(cmd) {
		  case PC_CMD_PCSTR: // start grub sens
			  f_start = 1;
			  HAL_UART_Transmit(&huart3, (uint8_t*)"$PSSTR,OK\r\n", 11, 200);
			  break;
		  case PC_CMD_PCSTP: // stop grubbing sens
			  f_start = 0;
			  HAL_UART_Transmit(&huart3, (uint8_t*)"$PSSTP,OK\r\n", 11, 200);
			  break;
		  case PC_CMD_PCRCH: // read channel number
			  if (f_start) break;
			  HAL_UART_Abort_IT(&huart1); HAL_UART_Abort_IT(&huart3);
			  uart_buffer_clear(&huart1); uart_buffer_clear(&huart3); HAL_Delay(5);

			  HAL_GPIO_WritePin(JDY_SET_GPIO_Port, JDY_SET_Pin, GPIO_PIN_RESET);
			  reset_JDY_40();

			  reset_chan_buf2();
			  HAL_UART_Transmit(&huart1, (uint8_t*)"AT+RFC\r\n", 8, 200); // send cmd READ CHANNEL to JDY-40
			  HAL_UART_Receive(&huart1, chan_buf2, 10, 200); // read answer from jdy-40
			  HAL_UART_Transmit(&huart3, chan_buf2, 10, 200);

			  HAL_GPIO_WritePin(JDY_SET_GPIO_Port, JDY_SET_Pin, GPIO_PIN_SET);

			  uart_start(&huart1); uart_start(&huart3); HAL_Delay(5);

			  // SEND: AT+RFC\r\n    ANSWER:  +RFC=001\r\n    =>   2B 52 46 43 3D 30 30 31 0D 0A
			  break;
		  case PC_CMD_PCSCH: // write channel number
			  if (f_start) break;
			  HAL_UART_Abort_IT(&huart1); HAL_UART_Abort_IT(&huart3);
			  uart_buffer_clear(&huart1); uart_buffer_clear(&huart3); HAL_Delay(5);

			  HAL_GPIO_WritePin(JDY_SET_GPIO_Port, JDY_SET_Pin, GPIO_PIN_RESET);
			  HAL_Delay(10);

			  chan_buf[0]='A'; chan_buf[1]='T'; chan_buf[2]='+';
			  chan_buf[3]='R'; chan_buf[4]='F'; chan_buf[5]='C';
			  chan_buf[9]=0x0d; chan_buf[10]=0x0a; chan_buf[11]=0;

			  HAL_UART_Transmit(&huart1, chan_buf, 11, 200); // send set channel cmd
			  HAL_UART_Receive(&huart1, chan_buf, 12, 200); // read answer from jdy-40

			  HAL_GPIO_WritePin(JDY_SET_GPIO_Port, JDY_SET_Pin, GPIO_PIN_SET);

			  chan_buf[2] = 0x0d; chan_buf[3] = 0x0a;
			  HAL_UART_Transmit(&huart3, chan_buf, 4, 200); // send the result to PC

			  uart_start(&huart1); uart_start(&huart3); HAL_Delay(5);

			  reset_JDY_40();
			  // SEND: AT+RFC123\r\n     ANSWER:  OK\r\n  (0x0d;0x0a)
			  break;
		  case PC_CMD_PCBAT: // battery level
			  HAL_UART_Transmit_DMA(&huart3, (uint8_t*)"$PSBAT,OK\r\n", 11);
			  break;
		  }
		  pc_len = 0;
	  }

	  if(_sys_tick > 500) {
		  _sys_tick = 0;
		  HAL_GPIO_TogglePin(GPIOC, GPIO_PIN_13); // blink 'cpu' led
	  }
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.HSEPredivValue = RCC_HSE_PREDIV_DIV1;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL9;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief USART1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART1_UART_Init(void)
{

  /* USER CODE BEGIN USART1_Init 0 */

  /* USER CODE END USART1_Init 0 */

  /* USER CODE BEGIN USART1_Init 1 */

  /* USER CODE END USART1_Init 1 */
  huart1.Instance = USART1;
  huart1.Init.BaudRate = 9600;
  huart1.Init.WordLength = UART_WORDLENGTH_8B;
  huart1.Init.StopBits = UART_STOPBITS_1;
  huart1.Init.Parity = UART_PARITY_NONE;
  huart1.Init.Mode = UART_MODE_TX_RX;
  huart1.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart1.Init.OverSampling = UART_OVERSAMPLING_16;
  if (HAL_UART_Init(&huart1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART1_Init 2 */

  /* USER CODE END USART1_Init 2 */

}

/**
  * @brief USART3 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART3_UART_Init(void)
{

  /* USER CODE BEGIN USART3_Init 0 */

  /* USER CODE END USART3_Init 0 */

  /* USER CODE BEGIN USART3_Init 1 */

  /* USER CODE END USART3_Init 1 */
  huart3.Instance = USART3;
  huart3.Init.BaudRate = 9600;
  huart3.Init.WordLength = UART_WORDLENGTH_8B;
  huart3.Init.StopBits = UART_STOPBITS_1;
  huart3.Init.Parity = UART_PARITY_NONE;
  huart3.Init.Mode = UART_MODE_TX_RX;
  huart3.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart3.Init.OverSampling = UART_OVERSAMPLING_16;
  if (HAL_UART_Init(&huart3) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART3_Init 2 */

  /* USER CODE END USART3_Init 2 */

}

/**
  * Enable DMA controller clock
  */
static void MX_DMA_Init(void)
{

  /* DMA controller clock enable */
  __HAL_RCC_DMA1_CLK_ENABLE();

  /* DMA interrupt init */
  /* DMA1_Channel2_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA1_Channel2_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(DMA1_Channel2_IRQn);

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOD_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOC, GPIO_PIN_13, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOA, JDY_PW3_Pin|JDY_PW2_Pin|JDY_SET_Pin, GPIO_PIN_SET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(JDY_PW1_GPIO_Port, JDY_PW1_Pin, GPIO_PIN_SET);

  /*Configure GPIO pin : PC13 */
  GPIO_InitStruct.Pin = GPIO_PIN_13;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

  /*Configure GPIO pins : JDY_PW3_Pin JDY_PW2_Pin JDY_SET_Pin */
  GPIO_InitStruct.Pin = JDY_PW3_Pin|JDY_PW2_Pin|JDY_SET_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pin : JDY_PW1_Pin */
  GPIO_InitStruct.Pin = JDY_PW1_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(JDY_PW1_GPIO_Port, &GPIO_InitStruct);

}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
