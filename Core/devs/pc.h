/*
 * pc.h
 *
 *  Created on: Feb 11, 2021
 *      Author: ssdmt@mail.ru
 */

#ifndef SRC_DEVS_PC_H_
#define SRC_DEVS_PC_H_


enum e_cmd_type {
	PC_CMD_PCSTR = 1, // start receive gsens data
	PC_CMD_PCSTP,     // stop receive gsens data
	PC_CMD_PCSCH,     // SET CHANNEL - change channel
	PC_CMD_PCRCH,     // READ CHANNEL - read current channel number
	PC_CMD_PCBAT,     // read battery level
	PC_CMD_PCDB1,     // PC DEBUG ALGO_1
	PC_CMD_PCDB2,     // PC DEBUG ALGO_2
};

#endif /* SRC_DEVS_PC_H_ */
