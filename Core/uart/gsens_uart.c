/*
 * gnss_uart.c
 *
 *  Created on: Sep 18, 2021
 *      Author: ssdmt@mail.ru
 */
#include "main.h"
#include "uart_util.h"
#include <stdlib.h>
#include <math.h>

uint8_t gsens_buf[25] = {0};

void gsens_uart_parse(uint32_t len, UART_HandleTypeDef *huart_pc);
int gnss_arg(uint8_t num, uint8_t l);

/* Desc: get GNSS sentence only
 * RET: sentence length value & setting insspd buffer
 */
uint8_t gsens_uart_cycle()
{
	uint8_t res = 0;
	static uint32_t i = 0;			// general cycle counter
	static uint8_t f_sentence = 0;	// uart capture sentence mode on
	uint8_t ch;

	ch = uart_char(UART_DEV_GSENS);

	if (ch == '$') {
		i = 0;
		f_sentence = 1;
		return 0;
	}

	if ((ch == '*')||(i > 21)) //  $-00.01,-00.01,-00.01* <-- 22symb
	{
		if (i==0) return 0;
		gsens_buf[i++] = 0x0d; gsens_buf[i] = 0x0a; // send to PC
		res = i+1; f_sentence = 0; i=0;
		return res; // buffer is COMPLETE !!!
	}

	if ((ch < 0x20)||(ch > 0x7e))
		return 0;

	if (f_sentence)
		gsens_buf[i++] = ch;

	return 0; // default - not complete
}

//extern uint8_t gsens_X[10];
//extern uint8_t gsens_Y[10];
//extern uint8_t gsens_Z[10];
//
//void gsens_uart_parse(uint32_t len, UART_HandleTypeDef *huart_pc /*DEBUG*/)
//{
//	int ii=0;
//	int cnt=0;
//
//	// X accel
//	ii++; cnt = 0;
//	while(gsens_buf[ii]!=',') gsens_X[cnt++] = gsens_buf[ii++];
//	gsens_X[cnt] = 0;
//
//	// Y accel
//	ii++; cnt = 0;
//	while(gsens_buf[ii]!=',') gsens_Y[cnt++] = gsens_buf[ii++];
//	gsens_Y[cnt] = 0;
//
//	// Z accel
//	ii++; cnt = 0;
//	while(gsens_buf[ii]!=',') gsens_Z[cnt++] = gsens_buf[ii++];
//	gsens_Z[cnt] = 0;
//
//	return 0;
//}
