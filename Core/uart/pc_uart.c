/*
 * pc_uart.c
 *
 *  Created on: Sep 18, 2021
 *      Author: ssdmt
 */

#include "main.h"
#include "uart_util.h"
#include "pc.h"

uint8_t pc_buffer[15] = {0};

uint8_t pc_uart_parse(uint32_t len, UART_HandleTypeDef *huart_pc);
int arg(uint8_t num, uint8_t *buffer, uint8_t l);

uint8_t pc_uart_cycle()
{
	uint8_t res = 0;
	static uint32_t ii = 0;			// general cycle counter
	static uint8_t f_sentence = 0;	// uart capture sentence mode on
	uint8_t ch;

	ch = uart_char(UART_DEV_PC);

	if (ch == '$') {
		ii = 0;
		f_sentence = 1;
		return 0;
	}

	if ((ch == '*')||(ii > 10))
	{
		pc_buffer[ii] = 0x0d; pc_buffer[ii+1] = 0x0a;
		res = ii+1; f_sentence = 0; ii=0;
		return res; // buffer is COMPLETE !!!
	}

	if ((ch < 0x20)||(ch > 0x7e))
		return 0; // not complete

	if (f_sentence)
		pc_buffer[ii++] = ch;

	return 0; // default - not complete
}


extern uint8_t chan_buf[12];

uint8_t pc_uart_parse(uint32_t len, UART_HandleTypeDef *huart_pc)
{
	uint8_t *s, res = 0;

	s = pc_buffer;

	if ((s[2]=='S')&&(s[3]=='T')&&(s[4]=='R'))
		res = PC_CMD_PCSTR;
	else if ((s[2]=='S')&&(s[3]=='T')&&(s[4]=='P'))
		res = PC_CMD_PCSTP;
	else if ((s[2]=='S')&&(s[3]=='C')&&(s[4]=='H'))
		res = PC_CMD_PCSCH;
	else if ((s[2]=='R')&&(s[3]=='C')&&(s[4]=='H'))
		res = PC_CMD_PCRCH;
	else if ((s[2]=='B')&&(s[3]=='A')&&(s[4]=='T'))
		res = PC_CMD_PCBAT;

	if (res == PC_CMD_PCSCH) { // [5-7] positoin
		chan_buf[6]=s[5]; chan_buf[7]=s[6]; chan_buf[8]=s[7];
	}

	return res;
}













