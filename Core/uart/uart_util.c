/*
 * uart_util.c
 *
 *  Created on: Feb 11, 2021
 *      Author: ssdmt@mail.ru
 */

#include "main.h"
#include "uart_util.h"

//static struct uart_obj_s uart[UART_DEV_INSTANCES] __attribute__ ((aligned(4))) = {0};

static struct uart_obj_s uart[UART_DEV_INSTANCES] = {0};
static uint8_t uart_rx[5] = {0};

extern uint8_t uart_IT_run;

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
	uint8_t index, c;

	c = uart_rx[0];

	if (huart->Instance == UART_PC) index = UART_DEV_PC;
	else if (huart->Instance == UART_GSENS) index = UART_DEV_GSENS;

	uart[index].buf[uart[index].tail] = c;

	uart[index].tail++;
	if (uart[index].tail >= UART_BUF_SIZE)		// loop
		uart[index].tail = 0;
	if (uart[index].tail == uart[index].head) {
		uart[index].head = uart[index].tail+1;	// tail moves head
		if(uart[index].head >= UART_BUF_SIZE)	// loop
			uart[index].head = 0;
	}

	HAL_UART_Receive_IT(huart, uart_rx, 1); // loop interrupt
}

/////////////////// user api ///////////////////////////////////////

uint8_t uart_char(uart_device_t index)
{
	uint8_t res;

	if (uart[index].head == uart[index].tail) // uart buffer is empty
		return 0;

	// loc
	res = uart[index].buf[uart[index].head];
	uart[index].head++;
	if (uart[index].head >= UART_BUF_SIZE)
			uart[index].head = 0;
	// unloc

	return res;
}

void uart_start(UART_HandleTypeDef *huart)
{
	// for start interrupt only
	HAL_UART_Receive_IT(huart, uart_rx, 1);
}

void uart_stop(UART_HandleTypeDef *huart)
{
	HAL_UART_Abort_IT(huart);
}

void uart_ack(UART_HandleTypeDef *huart)
{
	HAL_UART_Transmit(huart, "OK\r\n", 4, 100);
}

void uart_buffer_clear(UART_HandleTypeDef *huart)
{
	uint8_t index =
			(huart->Instance == UART_PC) ? UART_DEV_PC : UART_DEV_GSENS;
	uart[index].head = 0;
	uart[index].tail = 0;
}


