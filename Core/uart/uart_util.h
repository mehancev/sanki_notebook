/*
 * uart_util.h
 *
 *  Created on: Feb 11, 2021
 *      Author: ssdmt@mail.ru
 */

#ifndef SRC_UTILS_UART_UTIL_H_
#define SRC_UTILS_UART_UTIL_H_

#define UART_GSENS USART1
#define UART_PC USART3

#define UART_BUF_SIZE	(100)

typedef enum uart_device {
	UART_DEV_GSENS,
	UART_DEV_PC,
	UART_DEV_INSTANCES // total
} uart_device_t;

struct uart_obj_s
{
	uint16_t head;
	uint16_t tail;
	char buf[UART_BUF_SIZE];		// 1024 byte
};


uint8_t uart_char(uart_device_t dev_index);

void uart_start(UART_HandleTypeDef *huart);
void uart_stop(UART_HandleTypeDef *huart);
//void uart_ack(UART_HandleTypeDef *huart);
void uart_buffer_clear(UART_HandleTypeDef *huart);

void LOG(uint8_t *text);

#endif /* SRC_UTILS_UART_UTIL_H_ */
